
#include <EEPROM.h>
int addrTop = 0;
int addrBottom = 1;

int thrTop = 0;
int thrBottom = 9999;

int aux;
int thr;

int movementVector [5];
int throttleVector [5];

int outThr;

double percentage;
boolean firstTime = true;

#define ECHOPIN 2      // Pin to receive echo pulse
#define TRIGPIN 3      // Pin to send trigger pulse
#define THRIN 6        // transmitter input throttle  
#define AUXIN 7        // transmitter input aux
#define THROUT 5       // output for controller


#define MAXHEIGHT 100  //cm = 1m

void setup() {
  pinMode(THRIN, INPUT);
  pinMode(AUXIN, INPUT);
  pinMode(ECHOPIN, INPUT);
  pinMode(TRIGPIN, OUTPUT);
  pinMode(THROUT, OUTPUT);

  Serial.begin(9600);
  
  int initialTop = EEPROM.read(addrTop) * 10;
  int initialBottom = EEPROM.read(addrBottom) * 10;

  Serial.print("saved BOTTOM:");
  Serial.println(initialBottom);
  Serial.print("saved TOP:");
  Serial.println(initialTop);
  
  if (initialTop < 20) {
    thrTop = 0;
  } else {
    thrTop = initialTop; 
  }
  if (initialBottom < 20) {
    thrBottom = 2500;
  } else {
    thrBottom = initialBottom; 
  }

  Serial.print("BOTTOM:");
  Serial.println(thrBottom);
  Serial.print("TOP:");
  Serial.println(thrTop);
  
}

void loop() {

  thr = pulseIn(THRIN, HIGH, 25000);
  aux = pulseIn(AUXIN, HIGH, 25000);
  if (aux > 1450) { //SETUP
    aux = 0;
  } else if (aux > 1000) { //stabilize mode
    aux = 1; 
  } else { // OFF
    aux = 2; 
  }
  
  if (aux == 0 ) {
    if (firstTime) {
      thrTop = 0;
      EEPROM.write(addrTop, 0);
      thrBottom = 2500;
      EEPROM.write(addrBottom, 250);
      firstTime = false;
    }
    Serial.print("SETUP throttle:");Serial.println(thr);      

    if (thrTop < thr) {
      thrTop = thr;
      EEPROM.write(addrTop, thrTop / 10);
    }
    if (thrBottom > thr) {
      thrBottom = thr;
      EEPROM.write(addrBottom, thrBottom / 10);
    }
    
    Serial.print("BOTTOM:");Serial.println(thrBottom);
    Serial.print("TOP:");Serial.println(thrTop);
    outThr = 0; //no flying
    
  } else if (aux == 1) {
    firstTime = true;
//    Serial.print("input throttle:");Serial.println(thr);
    
    //measure height                
    int distance = getDistance();
//    Serial.print("distance: ");Serial.println(distance);
    movementVector = [distance, movementVector[0],movementVector[1],movementVector[2],movementVector[3]];
    
    percentage = (double)(thr - thrBottom) / (thrTop - thrBottom);
//    Serial.print("percentage: ");Serial.println(percentage);
    
    //target height
    int target = (double)MAXHEIGHT * percentage;
    
    Serial.print("target movement (cm):");Serial.println(target - distance);
    
    if (target - distance > 0) {
//      if stable position, add throttle and keep the pace
      outThr = throttleVector[0] + ((thrTop - ThrBottom) / 50); //add 2%
      
    } else {
      outThr -= (int)STEP;
    }
    throttleVector = [outThr, throttleVector[0],throttleVector[1],throttleVector[2],throttleVector[3]];
    
    Serial.print("movement:");Serial.print(movementVector[2]);Serial.print(",");Serial.print(movementVector[1]);Serial.print(",");Serial.println(movementVector[0]);
    Serial.print("throttle:");Serial.print(throttleVector[2]);Serial.print(",");Serial.print(throttleVector[1]);Serial.print(",");Serial.println(throttleVector[0]);
  } else {  
    firstTime = true;
    outThr = thr; //no change
  }
  
  digitalWrite(THROUT, outThr);
  delay(100); // I put this here just to make the terminal 
              // window happier
              
}

int getDistance() {
  digitalWrite(TRIGPIN, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW);                   // Send pin low again
  return pulseIn(ECHOPIN, HIGH)/58;                        // Calculate distance from time of pulse
}
